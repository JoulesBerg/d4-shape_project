#imported as is, but specified and renamed in our code as "rectangle" instead of "Shapes.rectangle"
import Shapes.rectangle as rectangle
#imported from the folder shapes, called square in our program
from Shapes import square
#imported as is folder name and module name must both be listed
import Shapes.circle





widthstr = input("Enter Rectangle Width: ")
lengthstr = input("Enter Rectangle Length: ")
width = int(widthstr)
length = int(lengthstr)
#imported as is, but specified and renamed in our code as "rectangle" instead of "Shapes.rectangle"
rarea = rectangle.area(width, length)
rareastr = str(rarea)
print("The area of your rectangle is "+rareastr)

sqsidestr = input("Enter side length of square: ")
sqside = int(sqsidestr)
#imported from the folder shapes, still called square
sarea = square.area(sqside)
sareastr = str(sarea)
print("The area of your square is "+sareastr)

radiusstr = input("Enter Circle Radius: ")
radius = int(radiusstr)
#imported as is folder name and module name must be listed
carea = Shapes.circle.area(radius)
careastr = str(carea)
print("The area of your circle is "+careastr)